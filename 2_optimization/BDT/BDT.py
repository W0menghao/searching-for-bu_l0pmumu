from ROOT import (RDataFrame,TCanvas,TChain,TFile,TTree,TH1D)
from progress.bar import Bar
import ROOT
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import mplhep as hep
import matplotlib as mpl
import matplotlib.pyplot as plt

# get the data and mc path 
current_dir = os.path.dirname(os.path.abspath(__file__))
base_path = current_dir[:current_dir.rindex("/searching-for-bu_l0pmumu/") + len("/searching-for-bu_l0pmumu/")]
mc_filename=base_path+"0_preselection/data/2016/Bu2L0pmumu_2016_data_TMVA.root"
data_filename=base_path+"0_preselection/MC/2016/MC_Bu2L0pmumu_2016_signal_TMVA.root.root"
#load mc signal 
treename="DecayTree"
sig_df=RDataFrame(treename,mc_filename)
sig_df=sig_df.Define("PID","p_MC15TuneV1_ProbNNp*mum_MC15TuneV1_ProbNNmu*mup_MC15TuneV1_ProbNNmu")
#load the data
file = ROOT.TFile(data_filename)
tree = file.Get("DecayTree")
tree.SetAlias("PID","p_MC15TuneV1_ProbNNp*mum_MC15TuneV1_ProbNNmu*mup_MC15TuneV1_ProbNNmu")

selection_list={}
def roofit_sidedata(tree,method,track,selection_list,PID,draw_option=False):
    '''
    tree is the input data.
    method means "BDT" or  "MLP",but this only has "BDT".
    track is the track type of lambda0 ,and have "LL" and "DD".
    selection_list means array [],
    such as {"BDT":[0.15,3,0.05,5]} ,it means for "BDT" method,the [ BDT_cut_value_LL,Lp_TRACK_Type_LL,BDT_cut_value_DD,Lp_TRACK_Type_DD]
    and Lp_TRACK_Type_LL is 3 ,and Lp_TRACK_Type_DD is 5
    PID is the cut of PID 
    '''

    if track=="LL":
        Track_type_cut=selection_list[method][0]
        Track_type=selection_list[method][1]
    elif track=="DD":
        Track_type_cut=selection_list[method][2]
        Track_type=selection_list[method][3]
    else :
        return 0
    #Create histogram
    xmin = 5000
    xmax = 5600
    nbins = 60
    h1 = ROOT.TH1D("h1", "", nbins, xmin, xmax)
    tree.Draw("Bu_DTFL0_M[0]>>h1",  f"(Evaluate{method}out>{Track_type_cut})&&(Lp_TRACK_Type=={Track_type})&&PID>{PID}&&(Bu_DTFL0_M[0]>5350||Bu_DTFL0_M[0]<5200)&&Bu_DTFL0_M[0]<5600", "goff")
    #Create RooFit variables
    mass = ROOT.RooRealVar("mass", "", xmin, xmax)
    mass.setRange("left", 5000, 5200)
    mass.setRange("right", 5350,5600)
    mass.setRange("signal", 5200, 5350)

    data = ROOT.RooDataHist("data", "", ROOT.RooArgList(mass), h1)
    k0 = ROOT.RooRealVar("k0", "", 1, -2, 2)
    k1=ROOT.RooRealVar("k1", "", 0, -2, 2)
    nbkg = ROOT.RooRealVar("nbkg", "", 1000, 0, 300000)

    ##\\\\\\\\\\\\\\\\RooChebychev can be changed
    pol = ROOT.RooChebychev("pol", "", mass, ROOT.RooArgList(k0))
    # pol = ROOT.RooChebychev("pol", "", mass, ROOT.RooArgList(k0,k1))
    ##\\\\\\\\\\\\\\\

    allpdf = ROOT.RooAddPdf("allpdf", "", ROOT.RooArgList(pol), ROOT.RooArgList(nbkg))
    result = allpdf.fitTo(data,ROOT.RooFit.Range("left,right"),ROOT.RooFit.PrintLevel(-1))
    #integral
    all_integral=pol.createIntegral(mass,mass,"left,right")
    integral_signal =  pol.createIntegral(mass,mass,"signal")
    bkg_midd_num=nbkg.getVal()*integral_signal.getVal()
    #draw
    if draw_option:
         # #Create frame and plot
        fr = mass.frame()
        fr.SetTitle(f"RooFit of #Lambda0 {track} ")
        fr.SetXTitle("B^{+}/[MeV]")
        fr.SetYTitle("Events")
        data.plotOn(fr)
        allpdf.plotOn(fr, ROOT.RooFit.Components("pol"),ROOT.RooFit.Range("left,right"), ROOT.RooFit.LineStyle(9), ROOT.RooFit.LineColor(ROOT.kBlue))
        allpdf.plotOn(fr, ROOT.RooFit.Components("pol"),ROOT.RooFit.Range("signal"), ROOT.RooFit.LineStyle(1), ROOT.RooFit.LineColor(ROOT.kRed))
        fr.BuildLegend()
        c1 = ROOT.TCanvas("c1",'', 800, 800)
        fr.Draw()
        plot_name=f"{method}_{track}_{PID}.png"
        c1.SaveAs(f"./fit_histogram/{plot_name}","recreate")
    return bkg_midd_num


def draw_result(array_efficency,draw_name):
    '''
    This is to save the result to csv and draw the result
    '''
    array=np.array(array_efficency)
    #make the result output more suitable
    optimization=pd.DataFrame(array,columns=['BDT','PID', 'FoM','eff','bkg'])
    optimization['PID'] = optimization['PID'].round(4)
    optimization['BDT'] = optimization['BDT'].round(4)
    optimization['FoM'] = optimization['FoM'].round(4)
    optimization['eff'] = optimization['eff'].round(4)
    optimization['bkg'] = optimization['bkg'].round(2)
    optimization.to_csv(f'./output_{track}.csv', sep='\t', index=False)
    # index =optimization[draw_name].idxmax()
    maxvalue=optimization[draw_name][index]
    plt.plot(BDT_cut, optimization[draw_name],marker='X', color='blue')
    plt.title(f"L0 {track} with BDT")
    plt.xlabel("BDT response",fontsize=15)
    if track=="LL":
        plt.title(f"L0 {track} with PID=0.40")
    else:
        plt.title(f"L0 {track} with PID=0.45")

    if draw_name=="eff":
        plt.ylabel("Efficency",fontsize=15)
    if draw_name=="FoM":
        plt.ylabel("F.O.M",fontsize=15)       
    if draw_name=="bkg":
        plt.ylabel("B",fontsize=15)    
    plt.scatter(BDT_cut[index],maxvalue , color='red', marker='+')
    label = "[{:.2f}, {:.4f}]".format(BDT_cut[index],maxvalue) 
    if draw_name=="bkg":
        label = "[{:.2f}, {:.0f}]".format(BDT_cut[index],maxvalue) 
    plt.text(BDT_cut[index],maxvalue , label, color='red', ha='right', va='top') 
    plt.grid()
    plt.show()
    plt.savefig(f"./BDT_{track}_{draw_name}.png")
    plt.close()



# selection_list means array [],
# such as {"BDT":[0.15,3,0.05,5]} ,it means for "BDT" method,the [ BDT_cut_value_LL,Lp_TRACK_Type_LL,BDT_cut_value_DD,Lp_TRACK_Type_DD]
# and  BDT_cut_value_LL is 0.15,Lp_TRACK_Type_LL is 3 ,and BDT_cut_value_DD is 0.05,  Lp_TRACK_Type_DD is 5

selection_list={"BDT":[0.15,3,0.05,5]}

# PID=p_MC15TuneV1_ProbNNp*mum_MC15TuneV1_ProbNNmu*mup_MC15TuneV1_ProbNNmu
## LL PID is > 0.40,## DD PID is > 0.45
# track is "LL" or "DD",method is "BDT",BDT_cut : (cut_start,cut_end,cut_gap)
###########
track="LL"
method="BDT"
BDT_cut= np.arange(-0.20,0.50,0.05)
PID_value=0.40

signal_sum= sig_df.Filter(f"EvaluateBDTout>-1&&PID>0.40&&Lp_TRACK_Type==3").Count().GetValue()
array=[]
for BDT_value in BDT_cut:
    cutstring="EvaluateBDTout>"+str(BDT_value)+f"&&Lp_TRACK_Type==3&&PID>{PID_value}"
    #MC efficency
    cut_num= sig_df.Filter(cutstring).Count().GetValue()
    eff=cut_num/signal_sum
    #fit to find B
    selection_list["BDT"][0]=BDT_value
    midd_bkg=roofit_sidedata(tree,method,track,selection_list,PID_value)
    Fom=eff/(2.5+math.sqrt(midd_bkg))
    array.append([BDT_value,PID_value,Fom,eff,midd_bkg]) 
index=9
draw_result(array,"FoM")
draw_result(array,"eff")
draw_result(array,"bkg")

#############
track="DD"
method="BDT"
BDT_cut= np.arange(0.10,0.65,0.05)
PID_value=0.45
signal_sum= sig_df.Filter(f"EvaluateBDTout>-1&&PID>0.45&&Lp_TRACK_Type==5").Count().GetValue()
array=[]
for BDT_value in BDT_cut:
    cutstring="EvaluateBDTout>"+str(BDT_value)+f"&&Lp_TRACK_Type==5&&PID>{PID_value}"
    #MC array
    cut_num= sig_df.Filter(cutstring).Count().GetValue()
    eff=cut_num/signal_sum
    #fit to find B
    selection_list["BDT"][2]=BDT_value
    midd_bkg=roofit_sidedata(tree,method,track,selection_list,PID_value)
    Fom=eff/(2.5+math.sqrt(midd_bkg))
    array.append([BDT_value,PID_value,Fom,eff,midd_bkg]) 
index=8
draw_result(array,"FoM")
draw_result(array,"eff")
draw_result(array,"bkg")
