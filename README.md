# Searching for $B^{+}\rightarrow \bar{\Lambda^{0}}p\mu^{+}\mu^{-}$.



## Introduction

This is the document for searching $B^{+}\rightarrow \bar{\Lambda^{0}}p\mu^{+}\mu^{-}$.
And it mainly has the preselection ,tmva training ,optimization .
And this is the general framework, subject to further modification and optimization

## Strategy
- Normalization channel B+ ->$\bar{\Lambda^{0}}$ p J/ψ(->𝜇⁺𝜇⁻) with BF ~ 9 × 10⁻⁷
  Already studied by LHCb with 9fb⁻¹ data [arXiv:2210.10346]

- Using **B2DibaryonMuMuLine**, only 2016-2018 data available

- **Blinded analysis:** not touching the signal mass region (!Sideband) for now

- MVA tools for suppression of combinatorial backgrounds:
  - **Signal region:** M(𝜇⁺𝜇⁻) < 2950MeV
  - **B Mass:**|M($\bar{\Lambda^{0}}$ p𝜇⁺𝜇⁻) - 5279.34| > 100MeV
  - **Separate $\bar{\Lambda^{0}}$ LL and DD**

- Setting up MVA tools for background suppression:
  - **Input variables:** Considering as **PT, IPχ² of B⁺ and final state particles, vertex χ² / FD / DIRA of B⁺ and $\bar{\Lambda^{0}}$, isolation variables...**

  - **Background:** sideband data
  - **Signal:** MC data

## Usage 

- preselection

for 0_preselection/ ,there are files which contain the basic cut for data and mc,
and the background and signal for tmva input are also here.
In order to process the data, you need to do the following instructions
```
cd ./searching-for-bu_l0pmumu/0_preselection/
chmod 700 preselection.sh
./preselection.sh

```
Or you can refer to the contents of the "/0_preselection/preselection.sh" file and perform the operation yourself on the command line.

- TMVA

for 1_TMVA/ ,there are files which are for training with background and signal.
In order to train the data and mc, you need to do the following instructions.
You should in the folder "/searching-for-bu_l0pmumu/",and you need to do the following instructions.
```
cd ./1_TMVA/
chmod 700 tmva.sh
./tmva.sh

```
Or you can refer to the contents of the "/1_TMVA/tmva.sh" file and perform the operation yourself on the command line.


- optimization

for 2_optimization/ ,there are files which are for training with background and signal.
You should in the folder "/searching-for-bu_l0pmumu/",and you need to do the following instructions.
```
cd ./2_optimization/
chmod 700 optimization.sh
./optimization.sh

```
Or you can refer to the contents of the "/2_optimization/optimization.sh" file and perform the operation yourself on the command line.

## more information about dataset

- Trigger 
  - **L0 level :** MuonDecision_TOS || DiMuonDecision_TOS
  - **HLT1 level :** TrackMVADecision_TOS || TwoTrackMVADecision_TOS || TrackMuonDecision_TOS || DiMuonHighMassDecision_TOS ||DiMuonLowMassDecision_TOS
  - **HLT2 level :** TopoNbodyDecision_TOS || TopoMuNbodyDecision_TOS ||TopoMuMuNbodyDecision_TOS || DiMuonDetachedDecision_TOS || DiMuonDetachedJPsiDecision_TOS

- preselection  on  $B^{+}$, $\bar{\Lambda}^{0}$ , $\mu^{+}\mu^{-}$
  - B^{+}    
    - 5000 < M($\bar\Lambda^{0}p\mu^{+} \mu^{-}$) < 5700
  - $\bar{\Lambda}^{0}$    
    - 1105 < M($p\pi^{-}$) < 1130
  - MVA  
    - Bu_DTF_Lambda0_decayLength>0
    - L0_FDCHI2_OWNPV>0
    - L0_DIRA_OWNPV>0

- TMVA input 
    - The input background comes from the sideband data, and the specific selection is shown in the following table, and the input signal comes from MC.
    - The ratio of data used for training to testing is 1:1.
      - |M(B⁺)-5279.34|>100
      -  M(𝜇⁺𝜇⁻)<2950 
      - Separate training/testing for $\bar{\Lambda}^{0}$ LL & DD

