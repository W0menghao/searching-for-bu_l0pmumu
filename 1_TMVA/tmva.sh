

#! /bin/bash

# training the track of LL and DD 
python Training.py --year 2016 --track LL
echo "training the track of LL finished"
python Training.py --year 2016 --track DD
echo "training the track of DD finished"

# use the training model and deal with data
python Application_BDT.py --mode data --year 2016 --test False
python Application_BDT.py --mode mc --year 2016 --test False
#end