
from ROOT import (TMVA,TChain,TCut,TFile)
import sys, os
from optparse import OptionParser

##mkdir to accept the result
try: os.mkdir("mvaresult")
except: print("mvaresult folder already exists.")

##to load the file the year and track
parser = OptionParser()
parser.add_option("--year", dest="year", help="Which year?(16/17/18)  [default: %default].", default="16")
parser.add_option("--track", dest="track", help="Which kind track?(DD/LL)  [default: %default].", default="DD")
(options, args) = parser.parse_args()
year = options.year
track = options.track

##choose the input file and output file !!!!
bkgname=f"../0_preselection/data/{year}/Bu2L0pmumu_{year}_bkg.root"
signame=f"../0_preselection/MC/{year}/MC_Bu2L0pmumu_{year}_signal.root"
savename=f"./mvaresult/tmva_{year}_{track}.root"
dataloader_file=f"tmva_{year}_{track}"
## end

# Load the signal and background data and MC
# split into even and odd set

trackcutstr = dict()
trackcutstr["LL"] = "Lp_TRACK_Type==3&&Lpi_TRACK_Type==3"
trackcutstr["DD"] = "Lp_TRACK_Type==5&&Lpi_TRACK_Type==5"

sigchain=TChain("DecayTree")
bkgchain = TChain("DecayTree")
sigchain.Add(signame)
bkgchain.Add(bkgname)
sig_even_tree = sigchain.CopyTree("eventNumber%2==0&&"+trackcutstr[track])
bkg_even_tree = bkgchain.CopyTree("eventNumber%2==0&&"+trackcutstr[track])
sig_odd_tree = sigchain.CopyTree("eventNumber%2==1&&"+trackcutstr[track])
bkg_odd_tree = bkgchain.CopyTree("eventNumber%2==1&&"+trackcutstr[track])

#create a factory
outputfile=TFile.Open(savename, "recreate")
factoryOptions = "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification"
factory = TMVA.Factory("TMVAClassification", outputfile, factoryOptions)

#add variables
branches=[]
######## on Bu
branches.append(["Bu_PT", "B PT"])
branches.append(["log10(Bu_DOCA12)", "B log(DOCA12)"])
branches.append(["log10(Bu_IPCHI2_OWNPV)", "B log(IP#chi^{2})"])
branches.append(["log10(1-Bu_DIRA_OWNPV)", "B log(1-DIRA)"])
branches.append(["log10(Bu_DTF_chi2[0]/Bu_DTF_nDOF[0])", "B log(#chi^{2}/nodf) DTF"])
branches.append(["log10(Bu_DTF_decayLength[0]/Bu_DTF_decayLengthErr[0])", "B log(DLS) DTF"])
#on pid and finally particles
branches.append(["(log10(p_IPCHI2_OWNPV)+log10(Lp_IPCHI2_OWNPV)+log10(Lpi_IPCHI2_OWNPV)+log10(mup_IPCHI2_OWNPV)+log10(mum_IPCHI2_OWNPV))", "#sum_{daughter}log((IP)#chi^{2}OWNPV)"])
#on L0
branches.append(["log10(1-L0_DIRA_OWNPV)", "#Lambda^{0} log(1-DIRA)"])
if track == "DD":
    branches.append(["log10(L0_IPCHI2_OWNPV)", "#Lambda^{0} log(#chi^{2}_{IP} OWNPV)"])
if track == "LL":
    branches.append(["log10(Bu_DTF_Lambda0_decayLength[0]/Bu_DTF_Lambda0_decayLengthErr[0])", "#Lambda^{0} log(DLS)"])
    branches.append(["log10(L0_FDCHI2_OWNPV)", "#Lambda log(#chi^{2}FDOWNPV)"])


dataloader=TMVA.DataLoader(dataloader_file)
print(f"Please check the information:\n year == {year} \n track == {track}")
for branch in branches:
    print("-"*30)
    print(f"\t{branch[0]}\t{branch[1]}")
    dataloader.AddVariable(branch[0], branch[1], "", "F")

# Spectator used to split
# Avoid precision loss
sweight = 1.0 
bweight = 1.0 

dataloader.AddSignalTree(sig_even_tree, sweight, "Training")
dataloader.AddSignalTree(sig_odd_tree, sweight, "Test")
dataloader.AddBackgroundTree(bkg_even_tree, bweight, "Training")
dataloader.AddBackgroundTree(bkg_odd_tree, bweight, "Test")

sCut = TCut("Bu_PT>0&&L0_DIRA_OWNPV<1&&L0_DIRA_OWNPV>0&&L0_FDCHI2_OWNPV>0&&Bu_DTF_Lambda0_decayLength[0]>0")
splitmode = "nTrain_Signal={}:nTrain_Background={}:SplitMode=Block:NormMode=NumEvents:!V".format(sig_even_tree.GetEntries(sCut.GetTitle())-100, bkg_even_tree.GetEntries(sCut.GetTitle())-100)
dataloader.PrepareTrainingAndTestTree(sCut, sCut, splitmode)

# this can change the method for training
# this provides cuts,Fisher,MLP and BDT method,and MLP and BDT behave well

#factory.BookMethod(dataloader, TMVA.Types.kCuts,"Cuts","!H:!V:FitMethod=MC:EffSel:SampleSize=200000:VarProp=FSmart")
#factory.BookMethod(dataloader, TMVA.Types.kFisher,"Fisher","H:!V:Fisher:VarTransform=None:CreateMVAPdfs:PDFInterpolMVAPdf=Spline2:NbinsMVAPdf=50:NsmoothMVAPdf=10")
factory.BookMethod(dataloader, TMVA.Types.kMLP,"MLP","!H:!V:NeuronType=tanh:VarTransform=N:NCycles=500:HiddenLayers=N+5:TestRate=10:EpochMonitoring")
BDT_options={
	'DD':"!H:!V:NTrees=850:MinNodeSize=0.06:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.6:SeparationType=GiniIndex:nCuts=25",
	'LL':"!H:!V:NTrees=400:MinNodeSize=0.4:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.3:SeparationType=GiniIndex:nCuts=25"
}
factory.BookMethod(dataloader, TMVA.Types.kBDT,"BDT",BDT_options[track])

# Train and test the classifier
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

#indicate finish it
outputfile.Close()
print("==> Wrote root file: ",savename,".root\n")
print("==> TMVAClassification is done!\n")
print("==> Too view the results, launch the GUI:\n root -l -e 'TMVA::TMVAGui(\""+savename+"\")'\n")
del factory
del dataloader
