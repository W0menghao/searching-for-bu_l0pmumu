import ROOT 
from ROOT import TMath,TChain,TFile,TMVA,TTree
import os, sys
from optparse import OptionParser
from array import array
from progress.bar import Bar

try: os.mkdir("appication")
except: print("appication folder already exists.")

parser = OptionParser()
parser.add_option("--mode", dest="mode", help="data or mc or bkg  [default: %default].", default="data")
parser.add_option("--test", dest="test", help="whether test [default: %default].", default=False)
parser.add_option("--year", dest="year", help="which year  [default: %default].", default="16")
(options, args) = parser.parse_args()
mode = options.mode
test=options.mode
year = options.year

var1 = array("f",[0])
var2 = array("f",[0])
var3 = array("f",[0])
var4 = array("f",[0])
var5 = array("f",[0])
var6 = array("f",[0])
var7 = array("f",[0])
var8 = array("f",[0])
varDD1 = array("f",[0])
varLL1 = array("f",[0])
varLL2 = array("f",[0])
def read_LL(method,weightfile_LL):
    readerLL = TMVA.Reader('!Color:!Silent')
    readerLL.AddVariable("Bu_PT", var1)
    readerLL.AddVariable("log10(Bu_DOCA12)", var2)
    readerLL.AddVariable("log10(Bu_IPCHI2_OWNPV)", var3)
    readerLL.AddVariable("log10(1-Bu_DIRA_OWNPV)", var4)
    readerLL.AddVariable("log10(Bu_DTF_chi2[0]/Bu_DTF_nDOF[0])", var5)
    readerLL.AddVariable("log10(Bu_DTF_decayLength[0]/Bu_DTF_decayLengthErr[0])", var6)
    readerLL.AddVariable("(log10(p_IPCHI2_OWNPV)+log10(Lp_IPCHI2_OWNPV)+log10(Lpi_IPCHI2_OWNPV)+log10(mup_IPCHI2_OWNPV)+log10(mum_IPCHI2_OWNPV))", var7)
    readerLL.AddVariable("log10(1-L0_DIRA_OWNPV)", var8)
    # For Lambda LL
    readerLL.AddVariable("log10(Bu_DTF_Lambda0_decayLength[0]/Bu_DTF_Lambda0_decayLengthErr[0])", varLL1)
    readerLL.AddVariable("log10(L0_FDCHI2_OWNPV)", varLL2)
    readerLL.BookMVA(method, weightfile_LL)
    return readerLL


def read_DD(method,weightfile_DD):
    readerDD = TMVA.Reader('!Color:!Silent')
    readerDD.AddVariable("Bu_PT", var1)
    readerDD.AddVariable("log10(Bu_DOCA12)", var2)
    readerDD.AddVariable("log10(Bu_IPCHI2_OWNPV)", var3)
    readerDD.AddVariable("log10(1-Bu_DIRA_OWNPV)", var4)
    readerDD.AddVariable("log10(Bu_DTF_chi2[0]/Bu_DTF_nDOF[0])", var5)
    readerDD.AddVariable("log10(Bu_DTF_decayLength[0]/Bu_DTF_decayLengthErr[0])", var6)
    readerDD.AddVariable("(log10(p_IPCHI2_OWNPV)+log10(Lp_IPCHI2_OWNPV)+log10(Lpi_IPCHI2_OWNPV)+log10(mup_IPCHI2_OWNPV)+log10(mum_IPCHI2_OWNPV))", var7)
    readerDD.AddVariable("log10(1-L0_DIRA_OWNPV)", var8)
    # For Lambda DD
    readerDD.AddVariable("log10(L0_IPCHI2_OWNPV)", varDD1)
    readerDD.BookMVA(method, weightfile_DD)
    return readerDD


filelist=dict()
outputfilelist=dict()

# #change the input of data_all and mc signal
filelist["data"]=f"../0_preselection/data/{year}/Bu2L0pmumu_{year}_data.root"
filelist['mc']=f"../0_preselection/MC/{year}/MC_Bu2L0pmumu_{year}_signal.root"
# #change the output of data_all and mc signal
outputfilelist["data"]=f"../0_preselection/data/{year}//Bu2L0pmumu_{year}_data_TMVA.root"
outputfilelist['mc']=f"../0_preselection/MC/{year}/MC_Bu2L0pmumu_2016_signal_TMVA.root"
# #add the background and output
# filelist['bkg']="/zshare/menghao/data/Bu_pLambdamumu/data/2016/Bu2L0pmumu_2016_bkg.root"
# outputfilelist['bkg']="/zshare/menghao/data/Bu_pLambdamumu/data/2016/Bu2L0pmumu_2016_bkg_TMVA.root"
#change the outputfile of test pattern
test_outputfile=f"./application/BDT_{mode}.root"
#change the weightfile of LL and DD
weightfile_LL_BDT='./tmva_16_LL/weights/TMVAClassification_BDT.weights.xml'
weightfile_DD_BDT='./tmva_16_DD/weights/TMVAClassification_BDT.weights.xml'
readerLL_BDT=read_LL('BDT',weightfile_LL_BDT)
readerDD_BDT=read_DD('BDT',weightfile_DD_BDT)


chain=TChain("DecayTree")
chain.Add(filelist[mode])
EvaluateBDTout = array('d',[0])
Track_type = array('d',[0])
if test:
    outputfile=TFile.Open(test_outputfile, 'RECREATE' )
    outputfile.cd()
    outputtree = TTree('outputtree', 'DecayTree')
    outputtree.Branch('EvaluateBDTout',EvaluateBDTout, 'EvaluateBDTout/D')
    outputtree.Branch('Track_type',Track_type, 'Track_type')
else:
    outputfile=TFile.Open( outputfilelist[mode], 'RECREATE' )
    outputfile.cd()
    outputtree = chain.CloneTree(0)
    outputtree.Branch('EvaluateBDTout',EvaluateBDTout, 'EvaluateBDTout/D')



bar = Bar('Processing', max=chain.GetEntries())
for i in range(chain.GetEntries()):
    bar.next()
    chain.GetEntry(i)
    var1[0]=chain.GetLeaf("Bu_PT").GetValue()
    var2[0]=TMath.Log(chain.GetLeaf("Bu_DOCA12").GetValue())
    var3[0]=TMath.Log(chain.GetLeaf("Bu_IPCHI2_OWNPV").GetValue())
    var4[0]=TMath.Log(1-chain.GetLeaf("Bu_DIRA_OWNPV").GetValue())
    var5[0]=TMath.Log(chain.GetLeaf("Bu_DTF_chi2").GetValue(0)/chain.GetLeaf("Bu_DTF_nDOF").GetValue(0))  
    var6[0]=TMath.Log(chain.GetLeaf("Bu_DTF_decayLength").GetValue(0)/chain.GetLeaf("Bu_DTF_decayLengthErr").GetValue(0))  

    var7[0]=sum([TMath.Log(chain.GetLeaf("p_IPCHI2_OWNPV").GetValue()),
            TMath.Log(chain.GetLeaf("Lp_IPCHI2_OWNPV").GetValue()),
            TMath.Log(chain.GetLeaf("Lpi_IPCHI2_OWNPV").GetValue()),
            TMath.Log(chain.GetLeaf("mup_IPCHI2_OWNPV").GetValue()),
            TMath.Log(chain.GetLeaf("mum_IPCHI2_OWNPV").GetValue())
            ])

    var8[0]=TMath.Log(1-chain.GetLeaf("L0_DIRA_OWNPV").GetValue())
    # for Lambda DD
    varDD1[0]=TMath.Log(chain.GetLeaf("L0_IPCHI2_OWNPV").GetValue())
    # for lambda LL
    varLL1[0]=TMath.Log(chain.GetLeaf("Bu_DTF_Lambda0_decayLength").GetValue(0)/chain.GetLeaf("Bu_DTF_Lambda0_decayLengthErr").GetValue(0))
    varLL2[0]=TMath.Log(chain.GetLeaf("L0_FDCHI2_OWNPV").GetValue())
    # check the range of input if error NaN happened
    # track type
    Lptrack = chain.GetLeaf("Lp_TRACK_Type").GetValue()
    Lpitrack = chain.GetLeaf("Lpi_TRACK_Type").GetValue()
    if (Lpitrack==3 and Lptrack==3):
        EvaluateBDTout[0] = readerLL_BDT.EvaluateMVA('BDT')
        Track_type=3
    elif (Lpitrack==5 and Lptrack==5):
        EvaluateBDTout[0] = readerDD_BDT.EvaluateMVA('BDT')
        Track_type=5
    else:
        EvaluateBDTout[0] = -100
        Track_type=0

    outputtree.Fill()

bar.finish()
outputtree.Write()
outputfile.Close()