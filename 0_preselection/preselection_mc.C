{

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////// for basic  MC
#if 1
// reduce Bu2L0pmumu MC,the MC Bu2L0pmumu_2016M*.root data, and the selection is ""
    TChain *ch = new TChain("DecayTree");

    /////change the file of mc
    ch->Add("/eos/lhcb/wg/RD/B2L0pmumu/mc/Bu2L0pmumu_2016M*.root/B2L0pbarmumu/DecayTree");
    ////

    TCut trigmucut("(Bu_L0MuonDecision_TOS||Bu_L0DiMuonDecision_TOS)&&(Bu_Hlt1TrackMVADecision_TOS||Bu_Hlt1TwoTrackMVADecision_TOS||Bu_Hlt1TrackMuonMVADecision_TOS||Bu_Hlt1DiMuonHighMassDecision_TOS||Bu_Hlt1DiMuonLowMassDecision_TOS)&&(Bu_Hlt2Topo2BodyDecision_TOS||Bu_Hlt2Topo3BodyDecision_TOS||Bu_Hlt2Topo4BodyDecision_TOS||Bu_Hlt2TopoMu2BodyDecision_TOS||Bu_Hlt2TopoMu3BodyDecision_TOS||Bu_Hlt2TopoMu4BodyDecision_TOS||Bu_Hlt2TopoMuMu2BodyDecision_TOS||Bu_Hlt2TopoMuMu3BodyDecision_TOS||Bu_Hlt2TopoMuMu4BodyDecision_TOS||Bu_Hlt2DiMuonDetachedDecision_TOS||Bu_Hlt2DiMuonDetachedJPsiDecision_TOS)");
    TCut truthmatchCut("abs(Bu_TRUEID)==521&&abs(p_TRUEID)==2212&&abs(L0_TRUEID)==3122&&abs(mum_TRUEID)==13&&abs(mup_TRUEID)==13&&abs(Lp_TRUEID)==2212&&abs(Lpi_TRUEID)==211");
    TCut TrackCut("Lp_TRACK_Type==Lpi_TRACK_Type");
    TCut BuMassCut("Bu_DTFL0_M[0]>5000&&Bu_DTFL0_M[0]<5700");
    TCut L0MassCut("Bu_DTF_Lambda0_M>1105&&Bu_DTF_Lambda0_M<1130");

    TCut MVAcut("L0_DIRA_OWNPV>0&&Bu_DTF_Lambda0_decayLength[0]>0&&L0_FDCHI2_OWNPV>0");

    TFile *file = TFile::Open("./MC/2016/MC_Bu2L0pmumu_2016_signal.root", "RECREATE");
    TTree *tree = ch->CopyTree(trigmucut+TrackCut+truthmatchCut+L0MassCut+BuMassCut+MVAcut);
    tree->Write();
    file->Close();
#endif

}
