
#! /bin/bash

mkdir -p ./data/2016
mkdir -p ./MC/2016


#  You should  run   preselection_data.C before you run preselection_bkg.C

echo ".q" | root preselection_data.C
echo "the preselection of data finished"
echo ".q" | root preselection_bkg.C
echo "the preselection of background finished"

echo ".q" | root preselection_mc.C
echo "the preselection of mc finished"

#end